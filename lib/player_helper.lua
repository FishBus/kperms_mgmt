--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

player_helper.lua

Factorio does some unusual things when passing names/indexes to game.get_player.

This tries to work around them.
	
--]]

function getPlayer(pldata)
	--[[
	pldata is expected to be a player object, a player name, or a player index
	We'll then use this to find the appropriate player.
	--]]
	if type(pldata) == "number" then
		return getPlayerIndexed(pldata)
	elseif type(pldata) == "string" then
		return getPlayerNamed(pldata)
	elseif type(pldata) == "table" and pldata.name and pldata.index then
		return pldata
	end
	return nil
end

function getPlayerNamed(name)
	--[[
	game.players[....] is a table lookup by either index or name.
	If the parameter passed is a numeric string (which could be a player name)
	then it is treated like a number, and an incorrect lookup is done.
	This function works around this problem by checking if the name is numeric,
	and if it is, scan through the players to find the correct one.  If it's
	alphanumeric, then we can use the normal lookup.
	-- ]]
	local numname = tonumber(name)
	if numname then
		-- numeric username
		for idx, player in pairs(game.players) do
			if name == player.name then
				return player
			end
		end
	else
		return game.get_player(name)
	end
	return nil
end

function getPlayerIndexed(index)
	--[[
	game.players lookups will apparently search the entire array for a match.
	If a match is found in either player name or index, then it is returned.
	This is undesired behavior if we have an index, and know it's an index.
	So we'll want to verify that the player we get back has the expected index
	and if not, find the player with the correct index by iterating through
	game.players
	-- ]]
	local player = game.get_player(index)
	if player.index ~= index then
		player = nil
		for idx, gplayer in pairs(game.players) do
			if gplayer.index == index then
				return gplayer
			end
		end
	end
	return player
end

