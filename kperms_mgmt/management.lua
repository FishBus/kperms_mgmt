--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

--]]

local default_group_def = require "kperms_mgmt/default_definition"
local default_player_def = require "kperms_mgmt/default_players"

kperms_mgmt = {}

function kperms_mgmt.initMod(event)
	local event = remote.call('kperms', 'events', 'on_permission_registered')
	Event.register(event, kperms_mgmt.on_permission_registered)

	if not global.kperms_mgmt then
		global.kperms_mgmt = {
			group_settings = {
				Default = {
					has_all_perms = true,
				},
			},
			default_group = 'Default',
			player_defaults = {},
		}
		Event.on_next_tick(function() 
			Event.on_next_tick(function() 
				-- Make sure this happens two ticks out, since the first tick
				-- will have registrations happening.
				kperms_mgmt.loadModDefaults()
			end)
		end)
	end

	remote.call('kperms', 'register_mod_permission', 'kperm_mgmt', {
		'manage_players', 'manage_groups'
	})
end

function kperms_mgmt.on_mod_setting_changed(event)
	if event.setting_type == "runtime-per-user" then
	elseif event.setting_type == "runtime-global" then
		if event.setting == "kperms_mgmt-definition_string" then
			local player = getPlayer(event.player_index)
			player.print("WARNING: Overriding all permissions from string.")
			kperms_mgmt.loadStringSettings(settings.global['kperms_mgmt-definition_string'].value)
		end
	end
end

function kperms_mgmt.loadModDefaults()
	-- Source the mod defaults from one of several different sources:
	-- From file(s), primarily for use with scenario code
	-- From mod settings, primarily used with knowledgeable users
	-- From passed string.
	local gkms = global.kperms_mgmt
	if type(default_group_def) == "table" then
		game.print("DEBUG: setting group settings from default file")
		gkms.group_settings = table.deepcopy(default_group_def)
	end
	if type(default_player_def) == "table" then
		game.print("DEBUG: setting player settings from default file")
		gkms.player_defaults = table.deepcopy(default_player_def)
	end
	if settings.global and settings.global['kperms_mgmt-definition_string'] then
		game.print("DEBUG: setting group/player settings from mod settings")
		kperms_mgmt.loadStringSettings(settings.global['kperms_mgmt-definition_string'].value)
	end
	game.print("DEBUG: settings:" .. serpent.line(gkms))
end

function kperms_mgmt.loadStringSettings(str)
	if string.len(str) < 2 then
		return {false, "import string too short to be valid"}
	end
	local definition = string_to_table(str) -- try as base64/lzw/json data.
	if not definition then -- try it as straight json.
		definition = game.json_to_table(str)
	end
	if definition then
		if definition.group_settings and 
		   definition.default_group and
		   definition.player_defaults then
			local gkms = global.kperms_mgmt
		   	gkms.group_settings = table.deepcopy(definition.group_settings)
			gkms.default_group = definition.default_group
			gkms.player_defaults = table.deepcopy(definition.player_defaults)
			return {true}
		end
		return {false, "definition missing fields"}
	end
	return {false, "unable to parse import string"}
end

function kperms_mgmt.exportSettingsToJSON()
	return game.table_to_json(global.kperms_mgmt)
end

function kperms_mgmt.exportSettingsToString()
	return table_to_string(global.kperms_mgmt)
end

function kperms_mgmt.on_permission_registered(event)
	log("Event received: on_permission_registered")
	if game.print then
		game.print(serpent.block(event))
	end
	for group, settings in pairs(global.kperms_mgmt.group_settings) do
		if settings.has_all_perms then
			log("Adding permission to "..group..": ".. event.modname.."/"..event.permission)
			remote.call('kperms', 'add_group_permission', group, event.modname, event.permission)
		end
	end
end

function kperms_mgmt.assign_player_to_their_group(plidx)
	local player = getPlayer(plidx)
	local pdef = kperms_mgmt.playerDefault(player.name)
	remote.call('kperms', 'set_player_group', player.index, pdef.group)
end

function kperms_mgmt.playerDefault(name)
	local pdef = global.kperms_mgmt.player_defaults[name]
	if not pdef then
		global.kperms_mgmt.player_defaults[name] = {
			group = global.kperms_mgmt.default_group,
		}
		pdef = global.kperms_mgmt.player_defaults[name]
	end
	return pdef
end

function kperms_mgmt.on_player_created(event)
	-- lookup player in our name->group mapping, and add them to their group.
	kperms_mgmt.assign_player_to_their_group(event.player_index)
end

function kperms_mgmt.on_player_joined(event)
	-- lookup player in our name->group mapping, and add them to their group.
	kperms_mgmt.assign_player_to_their_group(event.player_index)
end


Event.register(Event.def("soft_init"), kperms_mgmt.initMod)
Event.register(defines.events.on_runtime_mod_setting_changed, kperms_mgmt.on_mod_setting_changed)
Event.register(defines.events.on_player_created, kperms_mgmt.on_player_created)
Event.register(defines.events.on_player_joined_game, kperms_mgmt.on_player_joined)
