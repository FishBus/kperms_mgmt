--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

Just a small gui window for getting the current export string.

--]]

require 'lib/kwidgets'

kpm_iogui = {}

function kpm_iogui.can_import(plidx)
	local player = getPlayer(plidx)
	if player.admin or remote.call('kperms', 'player_has_permission', plidx, 'kperm_mgmt', 'import') then
		return true
	end
	return false
end

function kpm_iogui.init(event)
	if not global.kpm_iogui then
		global.kpm_iogui = {
			import_strings = {},
		}
	end
	kw_newTabDialog('kpm_iogui', 
		{caption={'kperms_mgmt.iogui.title'}},
		{position='center', defaultTab='exstring'}, 
		function(dialog) -- instantiation.
			-- must do button connections *outside* of any render functions.
			dialog:addTab('exstring',
				{
					caption = {'kperms_mgmt.iogui.exstring.title'}, 
					tooltip = {'kperms_mgmt.iogui.exstring.tooltip'}
				},
				function(dialog, tab) -- tab instantiation
					
				end,
				function(player, dialog, container) -- tab render
					local output = container.add({
						name='stringbox',
						type="text-box", 
					})
					output.word_wrap = true
					output.read_only = true
					output.style.width = container.style.maximal_width
					output.style.height = container.style.maximal_height - 50
					output.text = kperms_mgmt.exportSettingsToString()
					output.focus()
					output.select_all()
					container.add({type='label', caption={'kperms_mgmt.iogui.ctlc_copy'}})
				end
			)
			dialog:addTab('exjson',
				{
					caption = {'kperms_mgmt.iogui.exjson.title'}, 
					tooltip = {'kperms_mgmt.iogui.exjson.tooltip'}
				},
				function(dialog, tab) -- tab instantiation
					
				end,
				function(player, dialog, container) -- tab render
					local output = container.add({
						name='stringbox',
						type="text-box", 
					})
					output.word_wrap = true
					output.read_only = true
					output.style.width = container.style.maximal_width
					output.style.height = container.style.maximal_height - 50
					output.text = kperms_mgmt.exportSettingsToJSON()
					output.focus()
					output.select_all()
					container.add({type='label', caption={'kperms_mgmt.iogui.ctlc_copy'}})
				end
			)
			dialog:addTab('import',
				{
					caption = {'kperms_mgmt.iogui.import.title'}, 
					tooltip = {'kperms_mgmt.iogui.import.tooltip'},
					showif_func = kpm_iogui.can_import,
				},
				function(dialog, tab) -- tab instantiation
					kw_connectButton('kperms_mgmt.iogui.btn_import', function(event)
						if not kpm_iogui.can_import(event.player_index) then return end
						local player = getPlayer(event.player_index)
						local button = event.element
						local textbox = button.parent['kperms_mgmt.iogui.import_field']
						local result = kperms_mgmt.loadStringSettings(textbox.text)
						if result[1] == true then
							player.print("Imported settings") -- NEEDS_LOCALE
						else
							player.print("Import Failed: "..serpent.line(result[2])) -- NEEDS_LOCALE
						end
					end)
					kw_connectText('kperms_mgmt.iogui.import_field', function(event)
						local player = getPlayer(event.player_index)
						local iostrings = global.kpm_iogui.import_strings
						iostrings[player.index] = event.element.text
					end)
				end,
				function(player, dialog, container) -- tab render
					local iostrings = global.kpm_iogui.import_strings
					if not iostrings[player.index] then
						iostrings[player.index] = kperms_mgmt.exportSettingsToJSON()
					end
					local input = container.add({
						name='kperms_mgmt.iogui.import_field',
						type="text-box", 
					})
					input.word_wrap = true
					input.style.width = container.style.maximal_width
					input.style.height = container.style.maximal_height - 50
					-- setting the text *after* resizing exhibits less visual
					-- wordwrap problems.
					input.text = iostrings[player.index]
					input.focus()
					kw_newButton(container, 'kperms_mgmt.iogui.btn_import', 
						{'kperms_mgmt.iogui.btn_import_name'}, 
						{'kperms_mgmt.iogui.btn_import_tooltip'}, nil, nil)
				end
			)
		end,
		function(player, dialog, container) -- dialog render
			-- on display
			--player.print("DEBUG: Trying to render server info")
		end
	)
end

local function toggleIOGui(event)
	local player = getPlayer(event.player_index)
	-- reset their input string, just in case.
	local iostrings = global.kpm_iogui.import_strings
	iostrings[player.index] = kperms_mgmt.exportSettingsToJSON()
	local dialog = kw_getWidget('kpm_iogui')
	dialog:toggleShow(player)
end


Event.register(Event.def("soft_init"), kpm_iogui.init)

commands.add_command('perms.io', '', toggleIOGui)
