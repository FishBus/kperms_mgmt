--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

If used with a scenario, this file would be expected to contain 
definitions for the groups and the default permissions for those groups.

In the mod, this file should be completely blank!

--]]
