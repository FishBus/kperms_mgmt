--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

Just a small gui window for getting the current export string.

--]]

require 'lib/kwidgets'

kpm_mgmtgui = {}

function kpm_mgmtgui.init(event)
	kw_newTabDialog('kpm_mgmtgui', 
		{caption={'kperms_mgmt.mgmtgui.title'}},
		{position='center', defaultTab='players'}, 
		function(dialog) -- window instantiation.
			dialog:addTab('players',
				{
					caption = {'kperms_mgmt.mgmtgui.players.title'}, 
					direction = 'horizontal',
					tooltip = {'kperms_mgmt.mgmtgui.players.tooltip'},
				},
				function(dialog, tab) -- tab instantiation

				end,
				function(player, dialog, container) -- tab render
					local flow = container.add({
						name = 'flow',
						type = 'flow',
						direction = 'horizontal',
					})
					-- list of groups
					local grouplist = flow.add({
						name = "grouplist",
						type = "list-box",
						items = remote.call('kperms', 'list_groups'),
					})
					grouplist.style.width = container.style.maximal_width / 2 - 10
					-- list of players within group
					local playerlist = flow.add({
						name = "playerlist",
						type = "list-box",
						items = {'Alice', 'Bob', 'Charlie'},
					})
					playerlist.style.width = container.style.maximal_width / 2 - 10
					-- buttons to move players (Maybe not "out"?)

					-- list of players not within group.
				end
			)
			dialog:addTab('groups',
				{
					caption = {'kperms_mgmt.mgmtgui.groups.title'}, 
					tooltip = {'kperms_mgmt.mgmtgui.groups.tooltip'},
				},
				function(dialog, tab) -- tab instantiation

				end,
				function(player, dialog, container) -- tab render

				end
			)
		end,
		function(player, dialog, container) -- dialog render
			-- on display
		end
	)
end



local function toggleMgmtGui(event)
	local player = getPlayer(event.player_index)
	local dialog = kw_getWidget('kpm_mgmtgui')
	dialog:toggleShow(player)
end


Event.register(Event.def("soft_init"), kpm_mgmtgui.init)

commands.add_command('perms.mgmt', '', toggleMgmtGui)
