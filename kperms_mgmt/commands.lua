--[[
Copyright 2019 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details

--]]

require 'lib/fishbus_util'

kperms_mgmt_cmd = {}

function kperms_mgmt_cmd.create_group(event)
	local params = parseParams(event.parameter)
	if params[1] and type(params[1]) == "string" then
		local player = getPlayer(event.player_index)
		local print = player and player.print or game.print
		local res = remote.call('kperms', 'create_group', params[1])
		if res[1] then
			print("Created group")
		else
			print("Failed to create group: " .. res[2])
		end
	end
end

function kperms_mgmt_cmd.delete_group(event)
	local params = parseParams(event.parameter)
	if params[1] and type(params[1]) == "string" then
		local player = getPlayer(event.player_index)
		local print = player and player.print or game.print
		local res = remote.call('kperms', 'delete_group', params[1])
		if res[1] then
			print("Deleted group")
		else
			print("Failed to delete group: " .. res[2])
		end
	end
end

function kperms_mgmt_cmd.list_groups(event)
	local print = game.print
	if event.player_index and getPlayer(event.player_index) then
		print = getPlayer(event.player_index).print
	end
	local groups = remote.call('kperms', 'list_groups')
	print(serpent.line(groups))
end

function kperms_mgmt_cmd.set_default_group(event)

end


-- Commands to add to the game:

local cmd = commands.add_command
cmd('perm.create_group', '', kperms_mgmt_cmd.create_group)
cmd('perm.delete_group', '', kperms_mgmt_cmd.delete_group)
cmd('perm.list_groups', '', kperms_mgmt_cmd.list_groups)
cmd('perm.set_default_group', '', kperms_mgmt_cmd.set_default_group)
