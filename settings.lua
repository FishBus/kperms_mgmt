data:extend({
	--
	-- global settings
	--
	{
		name = "kperms_mgmt-definition_string",
		type = "string-setting",
		setting_type = "runtime-global",
		default_value = "",
		allow_blank = true,
	},
})
