--[[
Copyright 2018 "Kovus" <kovus@soulless.wtf>
BSD 3-Clause license, see LICENSE for details
	
	KPermsMgmt - Permission manager for kperms

--]]

require 'lib/event_extend'
require 'lib/player_helper'

require 'kperms_mgmt/management'
require 'kperms_mgmt/commands'
require 'kperms_mgmt/gui-io'
require 'kperms_mgmt/gui-mgmt'
